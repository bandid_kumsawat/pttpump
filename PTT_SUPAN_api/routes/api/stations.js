var router = require('express').Router();
var mongoose = require('mongoose');
var Sensor = mongoose.model('Sensor');
var Station = mongoose.model('Station');

router.post('/find/:station',function(req, res, next){
    var qry = {};
    qry.station_id = req.params.station;
    Promise.all([      
        Station.find(qry)
    ]).then(function(result){
        var out = {}
        out.dec = result[0];
        return res.json(out)        

    }).catch(next);
})

router.post('/find/all',function(req, res, next){
    // req.params.station
    var qry = {};
    qry.station_id = {};
    Promise.all([      
        // Sensor.find(qry).sort({_id:-1}).limit(1),
        Station.find(qry)
    ]).then(function(result){
        var out = {}
        out.dec = result[1];
        // out.sesnor = result[0]
        return res.json(out)        
    }).catch(next);
})




router.post('/save', function(req, res, next) {

    var station_new = new Station();

    //for id
    station_new.station_id = req.body.station_id

    //for description
    station_new.dec_PT = req.body.dec_PT
    station_new.dec_TT = req.body.dec_TT
    station_new.dec_Load_cell = req.body.dec_Load_cell
    station_new.dec_Gas_Gun = req.body.dec_Gas_Gun
    station_new.dec_FT = req.body.dec_FT
    station_new.dec_POS = req.body.dec_POS
    //for type
    station_new.type_PT = req.body.type_PT
    station_new.type_TT = req.body.type_TT
    station_new.type_Load_cell = req.body.type_Load_cell
    station_new.type_Gas_Gun = req.body.type_Gas_Gun
    station_new.type_FT = req.body.type_FT
    station_new.type_POS = req.body.type_POS

    //for tag
    station_new.tag_PT = req.body.tag_PT
    station_new.tag_TT = req.body.tag_TT
    station_new.tag_Load_cell = req.body.tag_Load_cell
    station_new.tag_Gas_Gun = req.body.tag_Gas_Gun
    station_new.tag_FT = req.body.tag_FT
    station_new.tag_POS = req.body.tag_POS
    
    station_new.url_grafana = req.body.url_grafana


    station_new.main_page = req.body.main_page
    station_new.main_sum_page = req.body.main_sum_page
    station_new.img = req.body.img

    station_new.save().then(function(){
        return res.json({
            mes: "Seccess"
        });
    }).catch(next);
    
});

router.put('/', function(req, res, next){

    Promise.all([ 
        Station.findOne({"station_id":req.body.station_id}),
    ]).then(function(result){
        var tmp_msg  = result[0];
        // console.log(tmp_msg);


        // for type
        if(typeof req.body.type_FT !== 'undefined'){
            tmp_msg.type_FT = req.body.type_FT;
        }

        if(typeof req.body.type_Gas_Gun !== 'undefined'){
            tmp_msg.type_Gas_Gun = req.body.type_Gas_Gun;
        }

        if(typeof req.body.type_Load_cell !== 'undefined'){
            tmp_msg.type_Load_cell = req.body.type_Load_cell;
        }
        
        if(typeof req.body.type_TT !== 'undefined'){
            tmp_msg.type_TT = req.body.type_TT;
        }

        if(typeof req.body.type_PT !== 'undefined'){
            tmp_msg.type_PT = req.body.type_PT;
        }
        if(typeof req.body.type_POS !== 'undefined'){
            tmp_msg.type_POS = req.body.type_POS;
        }
        // if(typeof req.body.type_willpilot !== 'undefined'){
        //     tmp_msg.type_willpilot = req.body.type_willpilot;
        // }

        // for dec
        if(typeof req.body.dec_FT !== 'undefined'){
            tmp_msg.dec_FT = req.body.dec_FT;
        }

        if(typeof req.body.dec_Gas_Gun !== 'undefined'){
            tmp_msg.dec_Gas_Gun = req.body.dec_Gas_Gun;
        }

        if(typeof req.body.dec_Load_cell !== 'undefined'){
            tmp_msg.dec_Load_cell = req.body.dec_Load_cell;
        }

        if(typeof req.body.dec_TT !== 'undefined'){
            tmp_msg.dec_TT = req.body.dec_TT;
        }

        if(typeof req.body.dec_PT !== 'undefined'){
            tmp_msg.dec_PT = req.body.dec_PT;
        }
        if(typeof req.body.dec_POS !== 'undefined'){
            tmp_msg.dec_POS = req.body.dec_POS;
        }
        // if(typeof req.body.dec_willpilot !== 'undefined'){
        //     tmp_msg.dec_willpilot = req.body.dec_willpilot;
        // }

        // for tag
        if(typeof req.body.tag_FT !== 'undefined'){
            tmp_msg.tag_FT = req.body.tag_FT;
        }

        if(typeof req.body.tag_Gas_Gun !== 'undefined'){
            tmp_msg.tag_Gas_Gun = req.body.tag_Gas_Gun;
        }

        if(typeof req.body.tag_Load_cell !== 'undefined'){
            tmp_msg.tag_Load_cell = req.body.tag_Load_cell;
        }

        if(typeof req.body.tag_TT !== 'undefined'){
            tmp_msg.tag_TT = req.body.tag_TT;
        }

        if(typeof req.body.tag_PT !== 'undefined'){
            tmp_msg.tag_PT = req.body.tag_PT;
        }
        if(typeof req.body.tag_POS !== 'undefined'){
            tmp_msg.tag_POS = req.body.tag_POS;
        }
        // if(typeof req.body.tag_willpilot !== 'undefined'){
        //     tmp_msg.tag_willpilot = req.body.tag_willpilot;
        // }
        
        // url grafana
        if(typeof req.body.url_grafana !== 'undefined'){
            tmp_msg.url_grafana = req.body.url_grafana;
        }
        if(typeof req.body.main_page !== 'undefined'){
            tmp_msg.main_page = req.body.main_page;
        }
        if(typeof req.body.main_sum_page !== 'undefined'){
            tmp_msg.main_sum_page = req.body.main_sum_page;
        }
        if(typeof req.body.img !== 'undefined'){
            tmp_msg.img = req.body.img;
        }
    
        tmp_msg.save().then(function(err){
            console.log(err)
            return res.json(tmp_msg);
        })  
    }).catch(next);

    // Station.save().then(function(){
    //     return res.json({
    //         mes: "Seccess"
    //     });
    // }).catch(next);
});

module.exports = router;