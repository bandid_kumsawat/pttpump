var router = require('express').Router();
var mongoose = require('mongoose');

var EchoFile = mongoose.model('EchoFile');
var EchoRaw = mongoose.model('EchoRaw');



router.get('/',function(req, res, next){
    req.params.station
    var qry = {};
    // qry.station_id = req.params.station;
    Promise.all([      
        EchoRaw.find(),
    ]).then(function(result){
        var out = result

        return res.json(result)        

    }).catch(next);
    // return res.json({test:"1234"})
});


router.post('/',function(req, res, next){
    
    var qry = {};
    
    // for type
    if(typeof req.body.station_name !== 'undefined'){
        qry.station_name = req.body.station_name;
    }

    if(typeof req.body.site_name !== 'undefined'){
        qry.site_name = req.body.site_name;
    }

    
    Promise.all([      
        EchoRaw.find(qry),
    ]).then(function(result){
        var out = result[0]
        console.log(out.length)
        var ret =  {
            count:out.length,
            data:out
        }
        return res.json(ret)        

    }).catch(next);
    // return res.json({test:"1234"})
});

router.get('/:station/:file',function(req, res, next){

    // var pathFile = '/TWM/'+req.params.station+'/'+req.params.file;
    var pathFile = '/root/g_drive/'+req.params.station+'/'+req.params.file;

    var options = {
        // root: path.join(__dirname, 'g_sync/NPI-B Well Site/'),
        // dotfiles: 'deny',
        headers: {
          'x-timestamp': Date.now(),
          'x-sent': true
        }
    }
    
    return res.sendFile(pathFile,options,function(err){
        console.log(err);
        if (err) {
          next(err)
        } else {
          console.log('Sent:', pathFile)
        }
        
    });
});

module.exports = router;