<?php

$curl = curl_init();
$index = $_GET['index'];
$station = $index;
// if ($index == 1){
//   $station = "NPI-B04";
// }
// if ($index == 2){
//   $station = "NPI-A06";
// }
// if ($index == 3){
//   $station = "NPI-C01";
// }
// if ($index == 4){
//   $station = "NPI-C02";
// }
// if ($index == 5){
//   $station = "NPI-C03";
// }
// if ($index == 6){
//   $station = "NPI-C04";
// }
// if ($index == 7){
//   $station = "NPI-A10";
// }
// if ($index == 8){
//   $station = "NPI-A02";
// }
// if ($index == 9){
//   $station = "NPI-A07";
// }
// if ($index == 10){
//   $station = "NPI-B01";
// }
// if ($index == 11){
//   $station = "NPI-B05";
// }
// if ($index == 12){
//   $station = "NPI-B06";
// }


curl_setopt_array($curl, array(
  CURLOPT_URL => "http://128.199.247.187:8086/query?pretty=true",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "db=freq_data&q=select%20load%2Cpos%20from%20rdata%20where%20%28time%20%3E%20now%28%29%20-%2010s%29%20AND%20%28%22sid%22%20%3D%20%27" . $index . "%27%29",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/x-www-form-urlencoded"
  ),
));

$response = curl_exec($curl);

curl_close($curl);

?>
  <html>
  <head>
    <script src="./../bower_components/moment/min/moment.min.js"></script>
    <script src = "./../dist/js/moment.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      function plot_chart(data_set){
        var data = [],
            time = (new Date()).getTime(),
            i,min = parseInt(data_set.results[0].series[0].values[0][2]),max  = parseInt(data_set.results[0].series[0].values[0][2]);
        for (var i = 0;i < data_set.results[0].series[0].values.length;i++){
          if (max < parseInt(data_set.results[0].series[0].values[i][2])){
            max = parseInt(data_set.results[0].series[0].values[i][2])
          }
          if (min > parseInt(data_set.results[0].series[0].values[i][2])){
            min = parseInt(data_set.results[0].series[0].values[i][2])
          }
        }
        console.log(max);
        console.log(min);
        for (i = 0; i < data_set.results[0].series[0].values.length; i += 1) {
          data.push({
              x: parseInt(data_set.results[0].series[0].values[i][1]),
              y: (100-(parseInt(data_set.results[0].series[0].values[i][2]) - min) / max * 100)
          });
        }
        console.log("----------------- show data -----------------")
        console.log(JSON.stringify(data))
        console.log("----------------- show data -----------------")
        return data;
      }

      var datachart = <?php echo $response; ?>
      var data = plot_chart(datachart)
      // var datatetasdfdsf = moment(datachart.results[0].series[0].values[0][0]).format("DD MMMM YYYY, HH:MM:ss") + " - " + moment(datachart.results[0].series[0].values[0][datachart.results[0].series[0].values.length - 1]).format("DD MMMM YYYY, HH:MM:ss")
      // var datatetasdfdsf = moment(datachart.results[0].series[0].values[0][0] /** 2020-06-05T14:31:38.1Z */).format("DD MMMM YYYY, HH:MM:ss") + " - " + moment(datachart.results[0].series[0].values[datachart.results[0].series[0].values.length - 1][0]).format("DD MMMM YYYY, HH:MM:ss")
      // console.log(datachart.results[0].series[0].values.length)
      var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
      var now = new Date(datachart.results[0].series[0].values[0][0]);
      var t_now = new Date(now.getTime());
      var varrrrr = t_now.getDate()+' '+months[t_now.getMonth() - 1 ]+' '+t_now.getFullYear()+" , "+t_now.getHours()+":" + t_now.getMinutes() + ":" + t_now.getSeconds() + ""
      var datatetasdfdsf = "" 
      datatetasdfdsf += varrrrr + " - "
      var now = new Date(datachart.results[0].series[0].values[datachart.results[0].series[0].values.length - 1][0]);
      var t_now = new Date(now.getTime());
      var varrrrr = t_now.getDate()+' '+months[t_now.getMonth() - 1 ]+' '+t_now.getFullYear()+" , "+t_now.getHours()+":" + t_now.getMinutes() + ":" + t_now.getSeconds() + ""
      datatetasdfdsf += varrrrr
      console.log('==============================')
      console.log(datatetasdfdsf)
      console.log('==============================')
      var dataout = [['','']]
      for (var i = 0;i < data.length;i++){
        var temp = []
        temp.push(data[i].y / 100, data[i].x)
        dataout.push(temp)
      }
      console.log(dataout)

      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        
        console.log(datachart)
        var data = google.visualization.arrayToDataTable(dataout)

        var options = {
          title: 'Surface DynaGraph',
          curveType: 'function',
          legend: { position: 'right' },
          hAxis: {
            title: 'Positioner',
            format: '## %'
          },
          vAxis: {
            title: 'Load Cell',
            format: '#,### lbs.'
            
          },
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
      // document.getElementById("timetimeshow").textContent = "hello"
      console.log(moment(new Date(),'YYYY-MM-DD HH:mm:ss').add(7,"hours").format('YYYY-MM-DD HH:mm:ss'))
    </script>
  </head>
  <body>
  
	  <div style="width: 900px;"><h1><center> <?php echo $station; ?> <br><span id = 'timetimeshow' style = 'font-style: italic;font-size: 16px;font-weight: 400;'></span></center></h1></div>
    <div id="curve_chart" style="width: 900px; height: 500px"></div>
  </body>

  <script>
    document.getElementById("timetimeshow").textContent = datatetasdfdsf//"Time: " +moment(new Date(),'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')
  </script>
</html>
