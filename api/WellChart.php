<html>
  <head>
    <script src="./../bower_components/moment/min/moment.min.js"></script>
    <script src = "./../dist/js/moment.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      function plot_chart(data_set){
        var data = [],
            time = (new Date()).getTime(),
            i,min = parseInt(data_set.results[0].series[0].values[0][2]),max  = parseInt(data_set.results[0].series[0].values[0][2]);
        for (var i = 0;i < data_set.results[0].series[0].values.length;i++){
          if (max < parseInt(data_set.results[0].series[0].values[i][2])){
            max = parseInt(data_set.results[0].series[0].values[i][2])
          }
          if (min > parseInt(data_set.results[0].series[0].values[i][2])){
            min = parseInt(data_set.results[0].series[0].values[i][2])
          }
        }
        console.log(max);
        console.log(min);
        for (i = 0; i < data_set.results[0].series[0].values.length; i += 1) {
          data.push({
              x: parseInt(data_set.results[0].series[0].values[i][1]),
              y: (100-(parseInt(data_set.results[0].series[0].values[i][2]) - min) / max * 100)
          });
        }
        console.log("----------------- show data -----------------")
        console.log(JSON.stringify(data))
        console.log("----------------- show data -----------------")
        return data;
      }

      
      var data = <?php echo $_GET['json_well']; ?>
      // var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
      // var now = new Date(datachart.results[0].series[0].values[0][0]);
      // var t_now = new Date(now.getTime());
      // var varrrrr = t_now.getDate()+' '+months[t_now.getMonth() - 1 ]+' '+t_now.getFullYear()+" , "+t_now.getHours()+":" + t_now.getMinutes() + ":" + t_now.getSeconds() + ""
      // var datatetasdfdsf = "" 
      // datatetasdfdsf += varrrrr + " - "
      // var now = new Date(datachart.results[0].series[0].values[datachart.results[0].series[0].values.length - 1][0]);
      // var t_now = new Date(now.getTime());
      // var varrrrr = t_now.getDate()+' '+months[t_now.getMonth() - 1 ]+' '+t_now.getFullYear()+" , "+t_now.getHours()+":" + t_now.getMinutes() + ":" + t_now.getSeconds() + ""
      // datatetasdfdsf += varrrrr
      // console.log('==============================')
      // console.log(datatetasdfdsf)
      // console.log('==============================')
      var dataout = [['','']]
      for (var i = 0;i < data.length;i++){
        var temp = []
        // x = position
        // y = load cell
        // temp.push(Number(data[i].Loads), Number(data[i].Positions))
        temp.push(Number(data[i].Positions), Number(data[i].Loads))
        dataout.push(temp)
      }
      console.log(dataout)

      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        
        // console.log(datachart)
        var data = google.visualization.arrayToDataTable(dataout)

        var options = {
          title: 'Surface DynaGraph',
          curveType: 'function',
          legend: { position: 'right' },
          hAxis: {
            title: 'Positioner',
            // format: '## %'
          },
          vAxis: {
            title: 'Load Cell',
            // format: '#,### lbs.'
            
          },
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
      // document.getElementById("timetimeshow").textContent = "hello"
      console.log(moment(new Date(),'YYYY-MM-DD HH:mm:ss').add(7,"hours").format('YYYY-MM-DD HH:mm:ss'))
    </script>
  </head>
  <body>
  
	  <div style="width: 900px;"><h1><center> <?php echo $_GET['wellname']; ?>  <br><span id = 'timetimeshow' style = 'font-style: italic;font-size: 16px;font-weight: 400;'></span></center></h1></div>
    <div id="curve_chart" style="width: 900px; height: 500px"></div>
  </body>

  <script>
    // document.getElementById("timetimeshow").textContent = datatetasdfdsf//"Time: " +moment(new Date(),'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')
  </script>
</html>
