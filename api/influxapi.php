<?php
  $index = $_GET['index'];
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "http://128.199.247.187:8086/query?pretty=true",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "db=freq_data&q=select%20load%2Cpos%20from%20rdata%20where%20%28time%20%3E%20now%28%29%20-%2010s%29%20AND%20%28%22sid%22%20%3D%20%27".$index."%27%29",
    CURLOPT_HTTPHEADER => array(
      "Content-Type: application/x-www-form-urlencoded"
    ),
  ));

  $response = curl_exec($curl);

  curl_close($curl);
  echo $response;
?>