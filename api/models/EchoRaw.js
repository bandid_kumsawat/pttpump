var mongoose = require('mongoose');
// var uniqueValidator = require('mongoose-unique-validator');

var EchoRawSchema = new mongoose.Schema({

    file_name:{type: String,unique: true},
    site_name:String,
    shot_time:String,
    station_name:String,

},{timestamps: true});

mongoose.model('EchoRaw', EchoRawSchema);