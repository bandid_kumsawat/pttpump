<?php
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_PORT => "4000",
    CURLOPT_URL => "http://128.199.247.187:4000/api/users/login",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "{\"user\":{\"username\":\"".$_GET['username']."\", \"password\":\"".$_GET['password']."\"}}",
    CURLOPT_HTTPHEADER => array(
      "Content-Type: application/json",
      "Postman-Token: ffc90db3-086b-4272-b90a-06d4e78ed203",
      "X-Requested-With: XMLHttpRequest",
      "cache-control: no-cache"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
    echo $response;
  }
?>