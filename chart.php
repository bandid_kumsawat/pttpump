<?php 
  @session_start();
  $_SESSION['type'] = "user";
  $_SESSION['station_id'] = 1;
  /*if (isset($_SESSION['username'])){

  }else{
    header("location: login.php");
  }*/
?>
<!-- 
ชลจันทร์ พัทยา บีช รีสอร์ท (Cholchan Pattaya Beach Resort)
โรงแรมบางกอกพาเลส (Bangkok Palace Hotel)
โรงแรมเซ็นทาราและคอนเวนชันเซ็นเตอร์ อุดรธานี (Centara Hotel & Convention Centre Udon Thani) 
-->
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PTTEP IoT</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" type="text/css" href="engine0/style.css" />
  <!-- button toggle -->
  <link rel="stylesheet" type="text/css" href="button-toggle/css/bootstrap2-toggle.min.css" />
  <link rel="stylesheet" type="text/css" href="button-toggle/css/bootstrap2-toggle.css  " />

  <!-- <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script> -->
  
  <!-- google font -->
  <link href="https://fonts.googleapis.com/css?family=Kanit|Noto+Sans&display=swap" rel="stylesheet">
</head> 
<style>
  * {
    font-family: 'Kanit', sans-serif;
    font-style: Thin;
  }
  body {
    font-family: 'Kanit', sans-serif;
    /* font-size: 16px; */
    background-image: url('./data0/images/Udon09.png');
    background-size: 100%;
    background-repeat: no-repeat;
    /* padding-top: 8%; */
  }
  #map-and-img-1 {
    padding: 0px 10px 0px 0px;
  }
  #map-and-img-2 {
    padding: 0px 0px 0px 10px;
  }
  @media only screen and (max-width: 960px) {
    /* For mobile phones: */
    #map-and-img-1 {
      padding: 0px 0px 10px 0px;
    }
    #map-and-img-2 {
      padding: 10px 0px 0px 0px;
    }
  }


    .switch {
      position: relative;
      display: inline-block;
      width: 60px;
      height: 34px;
    }

    .switch input { 
      opacity: 0;
      width: 0;
      height: 0;
    }

    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }

    .slider:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }

    input:checked + .slider {
      background-color: #a31640;
    }

    input:focus + .slider {
      box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
      border-radius: 34px;
    }

    .slider.round:before {
      border-radius: 50%;
    }
  </style>
<body class="sidebar-mini wysihtml5-supported fixed skin-blue">
<div style="font-weight:400;">
  <div class="wrapper">

    <header class="main-header">
      <a href="index.php" class="logo" style = "background-color:#0189be;">
        <span class="logo-mini"><b>IoT</b></span>
        <span class="logo-lg"><b>PTTEP IoT</b></span>
      </a>
      <nav class="navbar navbar-static-top" style = "background-color:#0189be;opacity: 10;">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="images/user<?php echo $_SESSION['station_id'];?>.jpg" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $_SESSION['username'];?> [<?php echo $_SESSION['type'];?>]</span>
              </a>
              <ul class="dropdown-menu">
                <li class="user-header" style = "background-color:rgb(108, 108, 108);">
                  <img src="images/user<?php echo $_SESSION['station_id'];?>.jpg" class="img-circle" alt="User Image">

                  <p>
                    <i class="fa fa-user"></i> <?php echo $_SESSION['username'];?> [<?php echo $_SESSION['type'];?>]
                    <small>[<?php echo $_SESSION['type'];?>]</small>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <!-- <a href="#" class="btn btn-default btn-flat">Profile</a> -->
                  </div>
                  <div class="pull-right">
                    <a href="session_destroy.php" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <aside class="main-sidebar">
      <section class="sidebar" >
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MENU</li>
          <li>
            <a href="index.php">
              <i class="fa fa-dashboard"></i> <span style = "font-size:16px;"><strong>Dashboard</strong></span>
            </a>
          </li>
          <li class="active treeview">
            <a href="#">
              <i class="fa fa-bar-chart"></i>
              <span style = "font-size:16px;"><strong>Statictics</strong></span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class='active'><a href="./chart.php" style = "font-size:16px;"><i class="fa fa-circle-o"></i> Chart</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-tv"></i>
              <span style = "font-size:16px;"><strong>Project</strong></span>
            </a>
          </li>
        </ul>
      </section>
    </aside>

    <div class="content-wrapper">


    <section class="content-header col-sm-12">
      
      <div class='box box-success'>
        <div class='box-header'>
        <i class="fa fa-star"></i>
          <h1 class='box-title' style = " font-size: 1.8em;padding-right:2%;">
            <!-- <strong>โรงแรมเซ็นทาราและคอนเวนชันเซ็นเตอร์ อุดรธานี (Centara Hotel & Convention Centre Udon Thani)</strong> -->
            <strong id = 'hotel_name'></strong>
          </h1>
        </div>
      </div>
    </section>


    <section class="content-header">
      <div class='container-fluid'>
        <div class='row'>
          <div class='col-md-8' id  = 'map-and-img-1'>
            <img id = "hotel_name" src="data0/images/<?Php echo $_SESSION['station_id'];?>.png" alt="bootstrap carousel" title="44921_15061113110029258650" width='100%' height="140em;"/>
          </div>
          <div class='col-md-4' id  = 'map-and-img-2'>
            <div id="map" style="height: 10em;"></div>
          </div>
        </div>
      </div>
    </section>

        <section class="content-header">
          <h1>
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        <section class="content">
          <div class="row">
            <section class="col-lg-12" id = 'section_box' >
                <div class='box box-primary'>
                  <div class='box-header'>
                    <i class='fa fa-circle' style = 'color: green;'></i>
                    <h3 class='box-title'></h3>
                  </div>
                  <div class='box-body'>
                    <div class='container-fluid'>
                      <div class='row'>
                         <div class='col-md-12'>

                        <iframe src="http://35.247.242.59/grafana/d/pMZAyS7Wz/energy-minitoring?orgId=1&from=1561005486236&to=1561006426794&kiosk=tv" style="border:none;" height = "600px" width = "100%"></iframe>
                        </div>
</div></div></div></div>
              </section>
          </div>
        </section>
    </div>

    <footer class="main-footer">
      <strong>DEAWARE TEAM</strong>
      <!-- <div class="pull-right hidden-xs">
        <strong>Energica IoT</strong> -->
      </div>
    </footer>
    <div class="control-sidebar-bg"></div>
  </div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- button toggle -->
<script type="text/javascript" src="button-toggle/js/bootstrap2-toggle.min.js"></script>
<!-- <script type="text/javascript" src="button-toggle/js/bootstrap2-toggle.js"></script> -->
<!-- Google Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA60weYi_4Yrt6C6GEiiiUegYnPBfWXaBk&callback=initMap" async defer></script>
<!-- underscore javascript -->
<script type="text/javascript" src="dist/js/underscore-min.js"></script>

<script language="JavaScript" >
  var time = 6;
  var map,table_s,marker;
  var obj;

  $( document ).ready(function() {
    try {
      first_load();
    } catch (e) {
      location.reload();
    }
  });
  function initMap(location) {
  map = new google.maps.Map(document.getElementById('map'), {
    center: location,
    zoom: 16,
    mapTypeId:  google.maps.MapTypeId.HYBRID 
  });
  var myLatLng = location;

  var image = {				
    url:'./images/mark.png',
  };

  var content_in_map = [
    '<strong>แหล่งผลิตน้ำมันดิบอู่ทองโครงการพีทีทีอีพี 1</strong>',
    //'<strong>โรงแรมบางกอกพาเลส</strong>',
    //'<strong>ชลจันทร์ พัทยา บีช รีสอร์ท</strong>'
  ];
  bounds = new google.maps.LatLngBounds();

  marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: "ที่ตั้งแหล่งผลิตน้ำมัน",
    icon: image,
  });

  bounds.extend(marker.position);

  info = new google.maps.InfoWindow({
    content: content_in_map[<?php echo $_SESSION['station_id']; ?> - 1]
  });
  
  // markers.push(marker);
  
  google.maps.event.addListener(marker, 'click', function() {
    info.open(map, marker);
  });
}


function first_load(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "./api/param_station.php?station_id=<?php echo $_SESSION['station_id'];?>",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
    }
  };

  $.ajax(settings).done(function (response) {
    obj = JSON.parse(response);
    console.log(obj);
    obj = _.sortBy(obj,'_id');

    // load hotel
    var content_in_map = [
      'แหล่งผลิตน้ำมันดิบอู่ทองโครงการพีทีทีอีพี 1',
      //'โรงแรมบางกอกพาเลส',
      //'ชลจันทร์ พัทยา บีช รีสอร์ท'
    ];
    var location_hotel = [
      {lat: 14.3765371, lng: 99.9729171},
      {lat: 13.752791, lng: 100.547281},
      {lat: 12.985936, lng: 100.919362}
    ];
    initMap(location_hotel[<?php echo $_SESSION['station_id'];?> - 1]);
    $("#hotel_name").text(content_in_map[ <?php echo $_SESSION['station_id']; ?> - 1]);
  });
}

</script>
</body>
</html>
