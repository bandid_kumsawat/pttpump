<!DOCTYPE html>
<html lang="en">
<head>
	<title>PTTEP Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/logo-pttep.ico"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="bower_components/vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="bower_components/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="bower_components/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="bower_components/vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="bower_components/vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="bower_components/vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="bower_components/css/util.css">
	<link rel="stylesheet" type="text/css" href="bower_components/css/main.css">
	<!--===============================================================================================-->
<script type="text/javascript">
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
    </script>
</head>
<body>

	<div class="limiter">
		<div class="container-login100" style="background-image: url('images/BGPTT_01.png');">
			<div class="wrap-login100 p-t-10 p-b-30">
				<!-- <form class="login100-form validate-form"> -->
				<div class="login100-form-avatar">
					<img src="images/logo-pttep3.png" alt="LOGO" height="100%" width="100%">
				</div>
				<span class="login100-form-title p-t-20 p-b-45">
					PTTEP 
				</span>		
				<div class="wrap-input100 validate-input m-b-10" data-validate = "Username is required">
					<!-- Username -->
					<input class="input100" type="text" name="username" id="username" placeholder="Username" value="">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
						<i class="fa fa-user"></i>
					</span>
				</div>

				<div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
					<!-- Password -->
					<input class="input100" type="password" name="pass" id="password" placeholder="Password" value="">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
						<i class="fa fa-lock"></i>
					</span>
				</div>

				<div class="container-login100-form-btn p-t-10 validate-form">
					<button class="login100-form-btn" onclick = "on_click()">
						Login
					</button>
				</div>

	</div>
</div>
</div>
<script src = "dist/js/moment.js"></script>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="module/checkToken.js"></script>
<script language="JavaScript" >
function on_click(){
	if ($("#username").val() != "" && $("#password").val() != ""){
		var settings = {
			"async": true,
			"crossDomain": true,
			"url": "api/param_login.php?username="+ $("#username").val() + "&password=" + $("#password").val(),
			"method": "GET",
			// "headers": {
			// 	"Content-Type": "application/json",
			// 	"X-Requested-With": "XMLHttpRequest",
			// 	"Accept": "*/*",
			// 	"Cache-Control": "no-cache",
			// 	"cache-control": "no-cache"
			// },
			// "processData": false,
			// "data": "{\"user\":{\"username\":\""+$("#username").val()+"\", \"password\":\""+$("#password").val()+"\"}}"
		}

		$.ajax(settings).done(function (response) {
			console.log(response);
			response = JSON.parse(response);
			if (response.user.data){
				var token = response.user.auth.token
				if (!check_token(token)){
					alert("Token คุณได้หมดอายุแล้วกรุณาล๊อกอินใหม่");
				}else{
					location.replace("./index.php?token=" + response.user.auth.token + "&username=" +response.user.auth.username + "&email=" + response.user.auth.email + "&type=" + response.user.auth.type + "&station_id=NPI-A02");
				}
			}else{
				alert("ไม่สามารถล๊อกอิน");
			}
		});
	}else{
		alert("กรุณากรอกข้อมูลให้ครบถ้วน");
	}
}
</script>
</body>
</html>
