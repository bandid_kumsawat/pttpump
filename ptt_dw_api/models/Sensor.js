var mongoose = require('mongoose');

var SchemaTypes = mongoose.Schema.Types;

var SensorSchema = new mongoose.Schema({
    station_id:Number, 
    PT:SchemaTypes.Decimal128,
    TT:SchemaTypes.Decimal128,
    Load_cell:SchemaTypes.Decimal128,
    Gas_Gun:SchemaTypes.Decimal128,
    FT:SchemaTypes.Decimal128,
});

mongoose.model('Sensor', SensorSchema);