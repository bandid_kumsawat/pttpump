<?php 
// IP          :: 128.199.247.187
// apache kafka
// spack
//  influx
// > drop database Rob_Pump_main
// > create  database Rob_Pump_main

// databases:   Rob_Pump_main
// measument:   ROB_STATION_1   for station 1
// measument:   ROB_STATION_2   for station 2
// measument:   ROB_STATION_3   for station 3
// measument:   ROB_STATION_4   for station 4
// measument:   ROB_STATION_5   for station 5

/*    --type=csv
      mongoexport --host localhost:27017 --username admin --password deaw1234 --authenticationDatabase admin --db PTT_PUMP --collection users --out users.json --jsonArray
      mongoexport --host localhost:27017 --username admin --password deaw1234 --authenticationDatabase admin --db PTT_PUMP --collection stations --out stations.json --jsonArray
      mongoexport --host localhost:27017 --username admin --password deaw1234 --authenticationDatabase admin --db PTT_PUMP --collection sensors --out sensors.json --jsonArray

      mongoexport --host localhost:27017 --username root --password deaw1234 --authenticationDatabase admin --db energica_iot --collection sensorlogs --out sensorlogs.json --type=csv
*/
// mqtt new    :: user : deaware  pass : deaw1234
// mqtt        :: user: iot pass: deaw1234 topic: sensor
// json format :: {"station_id":1,"PT":32.2,"TT":1.34,"Load_cell":22.4,"Gas_Gun":6.6}

// web         :: http://35.228.187.2/ptt
// user        :: admin pass :: deaw1234
// api & mondo :: www.energicaiot.com 
// grafana     :: http://35.247.242.59/grafana
// influx      :: 35.247.242.59
// FT : m3/s.
// PT : psi
// TT : C
// Load Cell : lb.
// Gas Gun : psi
// @session_start();
  if (!isset($_GET['token'])){
    header("location: login.php");
  }
    // $_GET['type'] = "Admin";
    // $_GET['station_id'] = 1;
    // $_GET['username'] = "ADMIN_PTT";
  // }else{
  //   // header("location: login.php");
  // }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="icon" type="image/png" href="images/icons/logo-pttep.ico"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PTTEP IoT</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" type="text/css" href="engine0/style.css" />
  <!-- button toggle -->
  <!-- <link rel="stylesheet" type="text/css" href="button-toggle/css/bootstrap2-toggle.min.css" /> -->
  <!-- <link rel="stylesheet" type="text/css" href="button-toggle/css/bootstrap2-toggle.css  " /> -->
  <!-- <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script> -->
  
  <!-- google font -->
  <link href ="https://fonts.googleapis.com/css?family=Kanit|Noto+Sans&display=swap" rel="stylesheet">

    <!-- calendar -->
    <link href='packages/core/main.css' rel='stylesheet' />
    <link href='packages/daygrid/main.css' rel='stylesheet' />
    <link href='packages/timegrid/main.css' rel='stylesheet' />
    <script src='packages/core/main.js'></script>
    <script src='packages/interaction/main.js'></script>
    <script src='packages/daygrid/main.js'></script>
    <script src='packages/timegrid/main.js'></script>
    <script src = "dist/js/calender_lib.js"></script>
    <script src = "dist/js/site_bar.js"></script>
    <!-- end calendar -->

</head> 
<style>
  * {
    font-family: 'Kanit', sans-serif;
    font-style: Thin;
  }
  .echo-download{
      margin: 20px !important; 
      color: #0523f3 !important;
      font-weight: 800;
      font-size: 20px;
      text-decoration: underline;
      text-decoration-color: #0523f3;
    }
  body {
    font-family: 'Kanit', sans-serif;
    /* font-size: 16px; */
    background-image: url('./data0/images/Udon09.png');
    background-size: 100%;
    background-repeat: no-repeat;
    /* padding-top: 8%; */
  }
  #map-and-img-1 {
    padding: 0px 10px 0px 0px;
  }
  #map-and-img-2 {
    padding: 0px 0px 0px 10px;
  }
  @media only screen and (max-width: 960px) {
    /* For mobile phones: */
    #map-and-img-1 {
      padding: 0px 0px 10px 0px;
    }
    #map-and-img-2 {
      padding: 10px 0px 0px 0px;
    }
  }


    .switch {
      position: relative;
      display: inline-block;
      width: 60px;
      height: 34px;
    }

    .switch input { 
      opacity: 0;
      width: 0;
      height: 0;
    }

    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }

    .slider:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }

    input:checked + .slider {
      background-color: #a31640;
    }

    input:focus + .slider {
      box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
      border-radius: 34px;
    }

    .slider.round:before {
      border-radius: 50%;
    }

    
/* Device status active and inactive show color  */
  .active-color {
    color: #2f9605;
    -webkit-animation-name: acive_co; 
    -webkit-animation-duration: 0.5s; 
    animation-name: acive_co;
    animation-duration: 0.5s;
  }

  .inactive-color {
    color: #8d8b8c;
    -webkit-animation-name: inacive_co; 
    -webkit-animation-duration: 0.5s; 
    animation-name: inacive_co;
    animation-duration: 0.5s;
  }
  .box-active {
    color: #000000;
    padding-top : 3px;
    padding-left : 10px;
    padding-right : 10px;
    border-radius : 4px;
    opacity: 0.9;
    background-color: #2f9605;
    -webkit-animation-name: box_atcive_co; 
    -webkit-animation-duration: 0.5s; 
    animation-name: box_atcive_co;
    animation-duration: 0.5s;
  }
  @-webkit-keyframes box_atcive_co {
    from {background-color: #2f9605;}
    to {background-color: #50ac17;}
  }

  @keyframes box_atcive_co {
    from {background-color: #2f9605;}
    to {background-color: #50ac17;}
  }
  .box-inactive {
    color: #000000;
    padding-top : 3px;
    padding-left : 10px;
    padding-right : 10px;
    border-radius : 4px;
    opacity: 0.9;
    background-color: #8d8b8c;
    -webkit-animation-name: box_inatcive_co; 
    -webkit-animation-duration: 0.5s; 
    animation-name: box_inatcive_co;
    animation-duration: 0.5s;
  }
    @-webkit-keyframes box_inatcive_co {
    from {background-color: #aca9aa;}
    to {background-color: #8d8b8c;}
  }

  @keyframes box_inatcive_co {
    from {background-color: #aca9aa;}
    to {background-color: #8d8b8c;}
  }
  @-webkit-keyframes acive_co {
    from {color: #2f9605;}
    to {color: #50ac17;}
  }

  @keyframes acive_co {
    from {color: #2f9605;}
    to {color: #50ac17;}
  }
  @-webkit-keyframes inacive_co {
    from {color: #aca9aa;}
    to {color: #8d8b8c;}
  }

  @keyframes inacive_co {
    from {color: #aca9aa;}
    to {color: #8d8b8c;}
  }
 /* end  */
  #calendar {
    max-width: 800px;
    margin: 0 auto;
  }

  /* for calendar view mouse position */
  .fc-event,
  .fc-event:hover {
    cursor: pointer;
  }


  </style>
<body class="sidebar-mini wysihtml5-supported fixed skin-blue">
<div style="font-weight:400;">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="#" class="logo" style = "background-color:#0189be;">
        <span class="logo-mini"><b>IoT</b></span>
        <span class="logo-lg"><b>PTTEP IoT</b></span>
        
      </a>
      <nav class="navbar navbar-static-top" style = "background-color:#0189be;opacity: 10;">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button" style = ":hover {background: yellow}" >
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="images/user1.png" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $_GET['username'];?> [<?php echo $_GET['type'];?>]</span>
              </a>
              <ul class="dropdown-menu">
                <li class="user-header" style = "background-color:rgb(108, 108, 108);">
                  <img src="images/user<?php echo $_GET['station_id'];?>.png" class="img-circle" alt="User Image">

                  <p>
                    <i class="fa fa-user"></i> <?php echo $_GET['username'];?> [<?php echo $_GET['type'];?>]
                    <small>[<?php echo $_GET['type'];?>]</small>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <!-- <a href="#" class="btn btn-default btn-flat">Profile</a> -->
                  </div>
                  <div class="pull-right">
                    <a href="login.php" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
            
            
            
          </ul>
        </div>
      </nav>
    </header>

    <aside class="main-sidebar">
      <section class="sidebar" >
        <ul class="sidebar-menu" data-widget="tree">


          <li class="header">MENU</li>
          <!-- <li>
            <a href="index.php?token=<?php echo $_GET['token'] . "&email=" . $_GET['email'] . "&type=" . $_GET['type'] . "&station_id=" . $_GET['station_id'] . "&username=" . $_GET['username'];?>">
              <i class="fa fa-dashboard"></i> <span style = "font-size:16px;"><strong>DASHBOARD</strong></span>
            </a>
          </li> -->



          <!-- menu-open -->
          <li class="treeview menu-open" id = "menu-name-for-site">
            <a href="#">
              <i class="fa fa-dashboard"></i>
              <span style="font-size:16px;"><strong>DASHBOARD</strong></span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu" style="display: block;" id = "menu-for-side2">
              <!-- <li>
                <a href="sync.php?token=<?php echo $_GET['token'] . "&email=" . $_GET['email'] . "&type=" . $_GET['type'] . "&station_id=" . $_GET['station_id'] . "&username=" . $_GET['username'];?>" style="font-size:16px;">
                  <i class="fa fa-circle-o" style= "color: #b593f6;"></i>WELL – NPI-B05
                </a>
              </li> -->
            </ul>
          </li>


          <li class="active treeview menu-open" id = "menu-name-for-site">
            <a href="#">
              <i class="fa fa-file-text-o"></i>
              <span style="font-size:16px;"><strong>FILE ECHOMETER</strong></span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>

            <ul class="treeview-menu" style="display: block;" id = "menu-for-side">

            </ul>
            
          </li>

          <li class="treeview menu-open" id = "menu-name-for-site">
            <a href="#">
              <i class="fa fa-file-text-o"></i>
              <span style="font-size:16px;"><strong>Download Program</strong></span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>

            <ul class="treeview-menu" style="display: block;" id = "menu-for-side-detail">

              <li>
                <a style="font-size:16px;" href = 'https://echometer.com/Software'>
                  <i class="fa fa-circle-o" style= "color: #b593f6;"></i>Dowload TWM
                </a>
              </li>
                

            </ul>
            
          </li>

        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <div class="content-wrapper">

    <section class="content-header col-sm-12">
      
      <div class='box box-success'>
        <div class='box-header'>
        <i class="fa fa-star"></i>
          <h1 class='box-title' style = " font-size: 1.8em;padding-right:2%;">
            <!-- <strong>โรงแรมเซ็นทาราและคอนเวนชันเซ็นเตอร์ อุดรธานี (Centara Hotel & Convention Centre Udon Thani)</strong> -->
            <strong id = 'hotel_name'></strong>
          </h1>
        </div>
      </div>
    </section>
    

    <section class="content-header">
      <div class='container-fluid'>
        <div class='row'>
          <div class='col-md-8' id  = 'map-and-img-1'>
            <img src="data0/images/1.png" alt="bootstrap carousel" title="44921_15061113110029258650" width='100%' height="200px"/>
          </div>
          <div class='col-md-4' id  = 'map-and-img-2'>
            <!-- <div id="map" style="height: 10em;"></div> -->
            
            <img src = "./images/map/NPI-A10.png" width="100%" height="200px"/>

          </div>
        </div>
      </div>
    </section>

        <section class="content-header">
          <h1>
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        <section class="content">
          <div class="row">
            <section class="col-lg-12" id = 'section_box' >

              <div class='box box-primary'>
                <div class='box-header'>
                <span></span>
                <h3 class='box-title'><strong id = "file_title"></strong></h3>
                <div class="box-tools pull-right" id = "date"></div>
                </div>
                <div class='box-body'>

                  <div class='container-fluid'>
                    <div class='row'>
                      <div class='col-md-3'></div>
                      <div class='col-md-6'>
                        <!-- for select box here -->

                        <div class="form-group">
                          <center>
                            <label for="site-fot-select">SELECT SITE:</label>
                          </center>
                          <input hidden id = 'hidden-tag-site-group'/>
                          <select class="form-control" id="site-fot-select">
                          </select>
                        </div>


                      </div>
                      <div class='col-md-3'>

                      </div>


                    </div>
                    <div class='row'>
                      <div class='col-md-12'>

                        <div id='calendar'></div>

                      </div>
                    </div>
                  </div>

                </div>
              </div>


            </section>
          </div>
        </section>
      </div>
    <footer class="main-footer">
      <strong>DEAWARE TEAM</strong>
      <!-- <div class="pull-right hidden-xs">
        <strong>Energica IoT</strong>
      </div> -->
    </footer>
    <div class="control-sidebar-bg"></div>
  </div>
</div>




      <!-- modal for calender -->
      <form action = "./../api/calender.php" method = "POST" enctype='multipart/form-data'>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
                  <h2 class="modal-title" id = "title_call_12"></h2>
              </div>
              <div class="modal-body" id = "is_date"> <!-- id = "is_date" -->
                <div class='row'>
                  <div class='col-sm-12' >
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">FILE NAME</th>
                        <th scope="col">TIME</th>
                        <th scope="col">DOWNLOAD</th>
                      </tr>
                    </thead>
                    <tbody id = 'content_calendar'>

                    </tbody>
                  </table>
                  </div>
                  <div class='row'>
                    <div class='col-lg-10'></div>
                    <div class='col-lg-2'>
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                      <!-- <button type="submit" class="btn btn-primary">Save changes</button> -->
                    </div>
                  </div>
                </div>
              </div>
              </div>
            </div>
        </div>
        </div>
    </form>



<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>


<!-- underscore javascript -->
<script type="text/javascript" src="dist/js/underscore-min.js"></script>
<script src="module/checkToken.js"></script>

<!-- moment datetime -->
<script src = "dist/js/moment.js"></script>


<!-- highchart javascript -->
<!-- <script src="dist/js/highcharts.js"></script> -->

<!-- data mock -->
<script src="api/data.js"></script>


<script language="JavaScript" >

  var time = 6;
  var map,table_s,marker;
  var obj;
  var user_type = "<?php echo $_GET['type'];?>";
  $( document ).ready(function() {
    
    if (check_token('<?php echo $_GET['token'];?>')){

      // $("#file_title").text("<?PHP echo $_GET['site']; ?>")



      first_load();

      // calendar_isload();
      for_side_bar(
        "sync.php?token=<?php echo $_GET['token'] . "&email=" . $_GET['email'] . "&type=" . $_GET['type'] . "&station_id=" . $_GET['station_id'] . "&username=" . $_GET['username'];?>",
        "<?PHP echo $_GET['site']; ?>"
      );


      
      for_side_bar_2("index.php?token=<?php echo $_GET['token'] . "&email=" . $_GET['email'] . "&type=" . $_GET['type'] . "&username=" .  $_GET['username'];?>","<?php echo $_GET['station_id'] ?>","<?php echo $_GET["token"]; ?>");
      



      
      // file_well_site();
      
    }else{
      location.replace("./login.php");
    }
    try {

    } catch (e) {
      location.reload();
    }
  });
function initMap(location) {
  map = new google.maps.Map(document.getElementById('map'), {
    center: location,
    zoom: 16,
    mapTypeId:  google.maps.MapTypeId.HYBRID 
  });
  var myLatLng = location;

  var image = {				
    url:'./images/mark.png',
  };

  var content_in_map = [
    '<a href = "'+window.location.href+'"><strong>L54/43 Project - NPI-B wellsite</strong></a>',
  ];
  bounds = new google.maps.LatLngBounds();

  marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    animation: google.maps.Animation.DROP,
    title: "ที่ตั้งแหล่งผลิตน้ำมัน",
    icon: image,
  });

  bounds.extend(marker.position);

  info = new google.maps.InfoWindow({
    content: content_in_map[<?php echo $_GET['station_id']; ?> - 1]
  });
  
  // markers.push(marker);
  
  google.maps.event.addListener(marker, 'mouseover', function() {
    info.open(map, marker);
  });
}

function first_load(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "api/param_station.php?station_id=<?php echo $_GET['station_id'];?>",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
    }
  };

  $.ajax(settings).done(function (response) {
    obj = JSON.parse(response);
    console.log(obj)
    $("#hotel_name").text(obj.dec[0].main_page);
    // $("#file_title").text(obj.dec[0].main_sum_page)
  });
}


function file_well_site(){
  
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://wellmonitors.site/appapi/api/file",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
    }
  }

  $.ajax(settings).done(function (response) {
    pot_calender_js(response);
  }); 

}

function twm_file_download(fileName, station_Name){

  for (var i = 0;i < 2;i++){
    try{
      let promise = new Promise((resolve, reject) => {
        if (i == 0){
          downloadFile(null, station_Name + "/" + fileName);
        }else{
          // downloadFile(null, station_Name + "/" + (fileName.split('.')[0] + ".bwf"));
        }
      });
      let result = promise;
    }catch(err){
      console.log(err)
    }
  }



  // downloadFile(null, station_Name + "/" + fileName);
  // downloadFile(null, station_Name + "/" + (fileName.split('.')[0] + ".bwf"));
}



function downloadFile(data, fileName, type="text/plain") {
  const a = document.createElement("a");
  a.style.display = "none";
  document.body.appendChild(a);
  console.log("https://wellmonitors.site/appapi/api/file/" + fileName);
  a.href = "https://wellmonitors.site/appapi/api/file/" + fileName;
  // a.href = window.URL.createObjectURL(
  //   new Blob([data], { type })
  // );
  a.setAttribute("download", fileName);
  a.click();
  window.URL.revokeObjectURL(a.href);
  document.body.removeChild(a);
}


</script>
</body>
</html>
